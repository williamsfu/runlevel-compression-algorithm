import filecmp
import os
import random
import string
import unittest

from plaid import encode, decode, BITMAP_SIZE

__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__))
)


class TestPlaid(unittest.TestCase):
    def test_custom(self):
        original = 'test_original.txt'
        result = 'test_result.txt'

        with open(os.path.join(__location__, original), 'w') as f:
            f.write('9' * BITMAP_SIZE)

        encoded, _ = encode(original)
        # Obviously this is good compression since this is a run-level algorithm
        print('[custom] compressed len: {0} vs. {1}'.format(len(encoded), BITMAP_SIZE))

        with open(os.path.join(__location__, result), 'w'):
            decode(encoded, result)

        self.assertTrue(filecmp.cmp(original, result))
        os.remove(original)
        os.remove(result)

    def test_random(self):
        original = 'test_random_original.txt'
        result = 'test_random_result.txt'

        # Internal function to generate a random string of ascii characters
        def file_generator(size=BITMAP_SIZE, valid_chars=string.ascii_letters + string.digits):
            return ''.join(random.choice(valid_chars) for _ in range(size))

        with open(os.path.join(__location__, original), 'w') as f:
            f.write(file_generator())

        encoded, flag = encode(original)
        # Shows that we actually do worse than usual, with lots of entropy. However, hopefully we can assume
        # imgur art is not beautiful when it's a bunch of randomly generated characters!

        print('[random] compressed len: {0} vs. {1}'.format(len(encoded), BITMAP_SIZE))

        # I can also return a flag during encode() when result > BITMAP_SIZE; then we use raw input and never decode
        # E.g.
        if not flag:
            print("Encoded is longer than raw. Do not decode")
            os.remove(original)
            return

        with open(os.path.join(__location__, result), 'w'):
            decode(encoded, result)

        self.assertTrue(filecmp.cmp(original, result))
        os.remove(original)
        os.remove(result)
