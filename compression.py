__author__ = "William Fu"
__email__ = "williesfu@gmail.com"


BITMAP_SIZE = 100 * 100


def encode(filename):
    encoded = ''
    with open(filename, 'r') as infile:
        last_char = ''
        redundancy = 0
        c = infile.read(1)
        counter = 0
        while counter < BITMAP_SIZE and c:
            # Run level encoding check: increment our number of repetitions if these are equal
            if c == last_char:
                redundancy += 1
            # So c is not the same as the last character, check to see if redundancy is zero
            else:
                if not redundancy:
                    encoded += c
                    last_char = c
            # redundancy variable is nonzero, append that number
                else:
                    encoded += str(redundancy)
                    encoded += c
                    last_char = c
                    redundancy = 0
                # Use a non ascii character to denote that the previous character is a digit
                if c.isdigit():
                    encoded += chr(256)
            c = infile.read(1)
            counter += 1

        # Finally, after our while loop, we append any remaining redundant characters saved.
        if redundancy:
            encoded += str(redundancy)

    # In the case of high entropy, we can just use the raw input if the length of our new encoding is actually longer
    if len(encoded) > BITMAP_SIZE:
        return encoded, False
    return encoded, True


def decode(encoded, filename):
    with open(filename, 'w') as outfile:
        last_char = ''
        redundancy = '0'
        length = len(encoded)
        for i in range(length):
            c = encoded[i]

            # Check if a non-ascii character follows
            if c.isdigit() and (i == length - 1 or encoded[i+1] != chr(256)):
                redundancy += c
            elif c == chr(256):
                continue
            else:
                outfile.write(last_char * (int(redundancy) + 1))
                last_char = c
                redundancy = '0'

        # Finally, append any remaining characters
        outfile.write(last_char * (int(redundancy) + 1))
